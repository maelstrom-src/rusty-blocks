# Rusty Blocks

Rusty Blocks is a modular status bar for [dwm](https://dwm.suckless.org/) written in Rust.

Inspired by [dwmblocks](https://github.com/torrinfail/dwmblocks).
