use std::fmt;
// use std::error;  // See TODO below.

pub type Result<T> = std::result::Result<T, RustyError>;

#[derive(Debug)]
pub struct RustyError {
    inner: RustyErrorKind,
}

#[derive(Debug)]
pub enum RustyErrorKind {
    IoError(std::io::Error),
    RonParse(ron::error::Error),
    XdgBaseDirError(xdg::BaseDirectoriesError),
}

impl fmt::Display for RustyError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.inner)
    }
}

// TODO: Not sure if this is needed?
// impl error::Error for RustyError {
//     fn source(&self) -> Option<&(dyn error::Error + 'static)> {
//         match self.inner {
//             RustyErrorKind::IoError(ref err) => Some(err),
//             RustyErrorKind::RonParse(ref err) => Some(err),
//             RustyErrorKind::XdgBaseDirError(ref err) => Some(err),
//         }
//     }
// }

impl fmt::Display for RustyErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            RustyErrorKind::IoError(ref err) => write!(f, "{}", err),
            RustyErrorKind::RonParse(ref err) => write!(f, "{}", err),
            RustyErrorKind::XdgBaseDirError(ref err) => write!(f, "{}", err),
        }
    }
}

impl From<RustyErrorKind> for RustyError {
    fn from(inner: RustyErrorKind) -> RustyError {
        RustyError { inner }
    }
}

impl From<std::io::Error> for RustyError {
    fn from(inner: std::io::Error) -> RustyError {
        RustyErrorKind::IoError(inner).into()
    }
}

impl From<ron::error::Error> for RustyError {
    fn from(inner: ron::error::Error) -> RustyError {
        RustyErrorKind::RonParse(inner).into()
    }
}

impl From<xdg::BaseDirectoriesError> for RustyError {
    fn from(inner: xdg::BaseDirectoriesError) -> RustyError {
        RustyErrorKind::XdgBaseDirError(inner).into()
    }
}
