use std::default::Default;
use std::fmt;
use std::fs;
use std::fs::File;
use std::io::Write;
use std::path::Path;
use std::str::FromStr;

use ron::ser::{to_string_pretty, PrettyConfig};
use serde::{Deserialize, Serialize};

use xdg::BaseDirectories;

use crate::errors::{Result, RustyError};

const BASE_DIRECTORY_PREFIX: &str = "rusty_blocks";
const CONFIG_FILENAME: &str = "config.ron";

#[derive(Serialize, Deserialize, Debug)]
pub struct BlockConfig {
    pub update_interval_sec: Option<u32>,
    pub update_signal: Option<usize>,
    pub command: CommandConfig,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct CommandConfig {
    pub program: String,
    pub args: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub delimiter: Option<String>,
    pub status_prefix: Option<String>,
    pub status_suffix: Option<String>,
    pub status_max_length: Option<usize>,
    pub block_output_max_length: Option<usize>,
    pub blocks: Vec<BlockConfig>,
}

impl Config {
    pub fn load() -> Self {
        load()
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            delimiter: Some(String::from(" | ")),
            status_prefix: None,
            status_suffix: None,
            status_max_length: Some(255),
            block_output_max_length: Some(50),
            blocks: vec![BlockConfig {
                update_interval_sec: None,
                update_signal: None,
                command: CommandConfig {
                    program: String::from("uname"),
                    args: vec![String::from("-r")],
                },
            }],
        }
    }
}

impl fmt::Display for Config {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let pretty_config = PrettyConfig::new()
            .depth_limit(4)
            .separate_tuple_members(true);

        match to_string_pretty(self, pretty_config) {
            Ok(ron) => return write!(f, "{}", ron),
            Err(e) => panic!("rusty_blocks: problem serializing config: {:?}", e),
        };
    }
}

impl FromStr for Config {
    type Err = RustyError;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let config = ron::from_str(s)?;
        Ok(config)
    }
}

fn load() -> Config {
    load_from_file()
        .map_err(|err| eprintln!("rusty_blocks: error loading config: {:?}", err))
        .unwrap_or_default()
}

fn load_from_file() -> Result<Config> {
    let path = BaseDirectories::with_prefix(BASE_DIRECTORY_PREFIX)?;
    let config_path = path.place_config_file(CONFIG_FILENAME)?;
    if Path::new(&config_path).exists() {
        let contents = fs::read_to_string(config_path)?;
        let config: Config = Config::from_str(&contents)?;

        Ok(config)
    } else {
        let mut file = File::create(&config_path)?;

        let config = Config::default();
        let ron = config.to_string();

        file.write_all(&ron.as_bytes())?;

        Ok(config)
    }
}
