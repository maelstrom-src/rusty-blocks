use std::env;
use std::process;
use std::sync::atomic::{AtomicBool, AtomicUsize, Ordering};
use std::sync::Arc;
use std::thread;
use std::time;

use signal_hook::consts::signal::{SIGHUP, SIGUSR1};
use signal_hook::consts::TERM_SIGNALS;
use signal_hook::flag;
use signal_hook::iterator::exfiltrator::WithRawSiginfo;
use signal_hook::iterator::SignalsInfo;

use crate::config::{BlockConfig, CommandConfig, Config};
use crate::utils::truncate_bytes;
use crate::xwrapper::XWrapper;

const DEFAULT_DELIMITER: &str = "|";
const DEFAULT_STATUS_PREFIX: &str = "";
const DEFAULT_STATUS_SUFFIX: &str = "";
const DEFAULT_STATUS_MAX_LENGTH: usize = 255;
const DEFAULT_BLOCK_OUTPUT_MAX_LENGHT: usize = 50;
const DEFAULT_BLOCK_UPDATE_INTERVAL_SEC: u32 = 0;
const DEFAULT_BLOCK_UPDATE_SIGNAL: usize = 0;

const ENV_BUTTON: &str = "BUTTON";

struct Command {
    command: process::Command,
    args: Vec<String>,
}

impl Command {
    fn new(config: &CommandConfig) -> Self {
        Self {
            command: process::Command::new(&config.program),
            args: config.args.iter().map(|s| s.to_owned()).collect(),
        }
    }

    fn execute(&mut self) -> String {
        let args: Vec<&str> = self.args.iter().map(String::as_ref).collect();

        let output = match self.command.args(args).output() {
            Ok(o) => {
                if o.status.success() {
                    o.stdout
                } else {
                    o.stderr
                }
            }
            Err(e) => e.to_string().into_bytes(),
        };

        match String::from_utf8(output) {
            Ok(s) => s,
            Err(e) => e.to_string(),
        }
    }
}

struct Block {
    update_interval_sec: u32,
    update_signal: usize, // TODO: Verfify that signal is between SIGRTMIN and SIGRTMAX.
    command: Command,
}

impl Block {
    fn new(config: &BlockConfig) -> Self {
        Self {
            update_interval_sec: config
                .update_interval_sec
                .unwrap_or(DEFAULT_BLOCK_UPDATE_INTERVAL_SEC),
            update_signal: config.update_signal.unwrap_or(DEFAULT_BLOCK_UPDATE_SIGNAL),
            command: Command::new(&config.command),
        }
    }

    fn run(&mut self) -> String {
        let mut output = String::new();
        if self.update_signal > 0 {
            let signal = self.update_signal as u8;
            output.push(signal as char);
        }

        let cmd_output = self.command.execute();
        output.push_str(cmd_output.trim_end_matches(&['\n'][..]));
        output
    }
}

pub struct Rusty {
    xwrapper: XWrapper,
    delimiter: String,
    blocks: Vec<Block>,
    block_output_max_length: usize,
    status_prefix: String,
    status_suffix: String,
    status_max_length: usize,
    status_bar: Vec<String>,
    latest_status: String,
    signals: SignalsInfo<WithRawSiginfo>,
    termination_signal: Arc<AtomicBool>,
    config_reload_signal: Arc<AtomicBool>,
    block_signal: Arc<AtomicUsize>,
}

impl Default for Rusty {
    fn default() -> Self {
        let xwrapper = XWrapper::new();

        Self {
            xwrapper,
            delimiter: DEFAULT_DELIMITER.to_owned(),
            block_output_max_length: DEFAULT_BLOCK_OUTPUT_MAX_LENGHT,
            blocks: Vec::new(),
            status_prefix: DEFAULT_STATUS_PREFIX.to_owned(),
            status_suffix: DEFAULT_STATUS_SUFFIX.to_owned(),
            status_max_length: DEFAULT_STATUS_MAX_LENGTH,
            status_bar: Vec::new(),
            latest_status: String::new(),
            signals: SignalsInfo::<WithRawSiginfo>::new(&[SIGUSR1]).unwrap(),
            termination_signal: Arc::new(AtomicBool::new(false)),
            config_reload_signal: Arc::new(AtomicBool::new(false)),
            block_signal: Arc::new(AtomicUsize::new(0)),
        }
    }
}

impl Rusty {
    pub fn new() -> Self {
        let mut rusty: Self = Default::default();
        rusty.load_config();
        rusty
    }

    fn load_config(&mut self) {
        eprintln!("rusty_blocks: load config...");

        let config = Config::load();

        let delimiter = config.delimiter.as_deref().unwrap_or(DEFAULT_DELIMITER);
        self.delimiter = delimiter.to_owned();

        let block_output_max_length = config
            .block_output_max_length
            .unwrap_or(DEFAULT_BLOCK_OUTPUT_MAX_LENGHT);
        self.block_output_max_length = block_output_max_length;

        let config_blocks = &config.blocks;
        let blocks: Vec<Block> = config_blocks
            .iter()
            .map(|config_block| Block::new(config_block))
            .collect();
        self.blocks = blocks;
        self.status_bar.resize(self.blocks.len(), String::new());

        let status_prefix = config
            .status_prefix
            .as_deref()
            .unwrap_or(DEFAULT_STATUS_PREFIX);
        self.status_prefix = status_prefix.to_owned();

        let status_suffix = config
            .status_suffix
            .as_deref()
            .unwrap_or(DEFAULT_STATUS_SUFFIX);
        self.status_suffix = status_suffix.to_owned();

        let status_prefic_suffix_len = status_prefix.len() + status_suffix.len();

        let mut status_max_length = config
            .status_max_length
            .unwrap_or(DEFAULT_STATUS_MAX_LENGTH);
        if status_max_length < status_prefic_suffix_len {
            panic!("rusty_blocks: status max length is too small");
        }

        status_max_length -= status_prefic_suffix_len;
        self.status_max_length = status_max_length;

        eprintln!("rusty_blocks: configured");
    }

    fn run_blocks1(&mut self, count: i32, signal: usize) {
        eprintln!("rusty_blocks: count: {}, signal: {}", count, signal);

        for (i, block) in self.blocks.iter_mut().enumerate() {
            let update_signal = block.update_signal;
            let interval_sec = block.update_interval_sec;

            if (interval_sec != 0 && count.abs() as u32 % interval_sec == 0)
                || (update_signal != 0 && update_signal == signal)
                || count == -1
            {
                let raw_output = block.run();

                eprintln!(
                    "rusty_blocks: block: {}, raw output: {} ({})",
                    i,
                    raw_output,
                    raw_output.len()
                );

                let output = truncate_bytes(&raw_output, self.block_output_max_length);

                eprintln!(
                    "rusty_blocks: block: {}, output: {} ({})",
                    i,
                    output,
                    output.len()
                );

                let latest_output = &mut self.status_bar[i];
                if output != latest_output {
                    eprintln!("rusty_blocks: changing '{}' to '{}'", latest_output, output);

                    latest_output.clear();
                    latest_output.push_str(output);
                }
            }
        }
    }

    fn run_blocks(&mut self, count: i32) {
        self.run_blocks1(count, self.signal());
    }

    fn write_status(&mut self) {
        eprintln!("rusty_blocks: status_bar: {:?}", self.status_bar);

        let raw_status = self.status_bar.join(&self.delimiter);

        eprintln!(
            "rusty_blocks: raw status: {} ({})",
            raw_status,
            raw_status.len()
        );

        let mut status = truncate_bytes(&raw_status, self.status_max_length).to_string();

        eprintln!(
            "rusty_blocks: truncated status: {} ({})",
            status,
            status.len()
        );

        status.insert_str(0, &self.status_prefix);
        status.push_str(&self.status_suffix);

        eprintln!("rusty_blocks: final status: {} ({})", status, status.len());

        if status != self.latest_status {
            eprintln!("rusty_blocks: display new status: {}", status);

            self.xwrapper.store_name(&status);
            self.latest_status = status;
        } else {
            eprintln!("rusty_blocks: no status update");
        }
    }

    pub fn status_loop(&mut self) {
        self.setup_signals();

        self.run_blocks(-1);

        let mut count: i32 = 0;

        loop {
            self.run_blocks(count);
            count += 1;

            self.write_status();

            if self.terminated() {
                eprintln!("rusty_blocks: terminating...");
                break;
            }

            if self.reload_config() {
                eprintln!("rusty_blocks: reloading config...");
                self.load_config();
                self.run_blocks(-1);
            }

            for signal in self.signals.pending() {
                let si_value = unsafe { signal.si_value() };

                let sival_int = si_value.sival_ptr as usize;

                let button = sival_int & 0xff;
                let block_signal = sival_int >> 8;

                eprintln!(
                    "rusty_blocks: sival_int: {}, button: {}, block_signal: {}",
                    sival_int, button, block_signal
                );

                env::set_var(ENV_BUTTON, button.to_string());

                self.run_blocks1(count, block_signal);

                env::remove_var(ENV_BUTTON);
            }

            thread::sleep(time::Duration::from_millis(1000));
        }

        eprintln!("rusty_blocks: closing...");
        self.xwrapper.close();
    }

    fn terminated(&self) -> bool {
        self.termination_signal.load(Ordering::Relaxed)
    }

    fn reload_config(&self) -> bool {
        self.config_reload_signal.swap(false, Ordering::Relaxed)
    }

    fn signal(&self) -> usize {
        self.block_signal.swap(0, Ordering::Relaxed)
    }

    fn setup_signals(&self) {
        for sig in TERM_SIGNALS {
            // When terminated by a second term signal, exit with exit code 1.
            // This will do nothing the first time (because term_now is false).
            flag::register_conditional_shutdown(*sig, 1, Arc::clone(&self.termination_signal))
                .unwrap();
            // But this will "arm" the above for the second time, by setting it to true.
            // The order of registering these is important, if you put this one first, it will
            // first arm and then terminate ‒ all in the first round.
            flag::register(*sig, Arc::clone(&self.termination_signal)).unwrap();
        }

        // SIGHUP to reload configuration.
        flag::register(SIGHUP, Arc::clone(&self.config_reload_signal)).unwrap();

        // Setup block signals using real-time signals.
        let sigmin;
        let sigmax;
        unsafe {
            sigmin = __libc_current_sigrtmin();
            sigmax = __libc_current_sigrtmax();
        }

        eprintln!("rusty_blocks: sigmin: {}, sigmax: {}", sigmin, sigmax);

        let signals = (sigmin..=sigmax).collect::<Vec<_>>();
        for (i, signal) in signals.iter().enumerate() {
            flag::register_usize(*signal, Arc::clone(&self.block_signal), i + 1).unwrap();
        }
    }
}

// TODO when libc exposes this through their library and even better when the nix crate does we
// should be using that binding rather than a C-binding.
/// C bindings to SIGMIN and SIGMAX values
extern "C" {
    fn __libc_current_sigrtmin() -> i32;
    fn __libc_current_sigrtmax() -> i32;
}
