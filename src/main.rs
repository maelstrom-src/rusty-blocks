mod config;
mod errors;
mod rusty;
mod utils;
mod xwrapper;

const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    eprintln!("rusty_blocks v{}", VERSION);

    let mut rusty = rusty::Rusty::new();
    rusty.status_loop();
}
