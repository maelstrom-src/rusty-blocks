use std::ffi::CString;
use std::os::raw::*;
use std::ptr;

use x11_dl::xlib;

pub struct XWrapper {
    xlib: xlib::Xlib,
    display: *mut xlib::Display,
    root_window: xlib::Window,
}

impl XWrapper {
    pub fn new() -> Self {
        let xlib = xlib::Xlib::open().unwrap();
        let display = unsafe { (xlib.XOpenDisplay)(ptr::null()) };
        if display.is_null() {
            panic!("rusty_blocks: unable to open display");
        }

        let root_window = unsafe { (xlib.XDefaultRootWindow)(display) };

        Self {
            xlib,
            display,
            root_window,
        }
    }

    pub fn close(&self) {
        let result = unsafe { (self.xlib.XCloseDisplay)(self.display) };
        eprintln!("rusty_blocks: close result: {}", result);
    }

    pub fn store_name(&self, name: &str) {
        let name_str = CString::new(name).unwrap();
        let result = unsafe {
            (self.xlib.XStoreName)(
                self.display,
                self.root_window,
                name_str.as_ptr() as *mut c_char,
            )
        };

        eprintln!("rusty_blocks: store name reuslt: {}", result);

        // Flush required to display the status.
        self.flush();
    }

    fn flush(&self) {
        let result = unsafe { (self.xlib.XFlush)(self.display) };
        eprintln!("rusty_blocks: flush result: {}", result);
    }

    // TODO: Not sure if this is needed?
    // fn sync(&self) {
    //     let result = unsafe { (self.xlib.XSync)(self.display, xlib::False) };
    //     eprintln!("Sync result: {}", result);
    // }
}
