pub fn truncate_bytes(string: &str, max_bytes: usize) -> &str {
    if string.len() <= max_bytes {
        return string;
    }

    let mut index = max_bytes;
    while !string.is_char_boundary(index) {
        index -= 1;
    }
    &string[..index]
}
